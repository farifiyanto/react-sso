# This is sample of digital amoeba SSO

Credited to : vigarblock

Source Code: [auth0-end-to-end](https://github.com/vigarblock/auth0-end-to-end.git)

Modified By Me : Farid Arifiyanto

## Backend

run `cd dashboard-backend`

run `npm install`

edit redirectUri in index.js

run `node index.js`

## Client
run `cd dashboard-frontend`

run `npm install`

edit a href in LoginButton.js

edit clientID and clientSecret in Challenges.js

run `npm start`

