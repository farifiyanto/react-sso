var express = require("express");
var axios = require("axios");
var port = process.env.PORT || 3001;
var app = express();
const cors = require("cors");

// Allow other application to make HTTP request to application
var corsOptions = {
  origin: process.env.CORS_URL || "http://localhost:3000"
};
app.use(cors(corsOptions));


app.get("/api/loginSSO", async (req, res) => {
  try {
    const clientID = req.query.client_id;
    const clientSecret = req.query.client_secret;
    const requestToken = req.query.code;
    const redirectUri = "http://localhost:3000/challenges";
    const dataToken = await axios.post(`https://git.digitalamoeba.id/oauth/token?client_id=${clientID}&client_secret=${clientSecret}&code=${requestToken}&grant_type=authorization_code&redirect_uri=${redirectUri}`, {})
      .then(res => res.data);
    const token = dataToken.access_token;
    const dataProfile = await axios.get(`http://git.digitalamoeba.id/api/v4/user?access_token=${token}`, {}).then(res => res.data);
    let profile = [] ;
    profile.push({username : dataProfile.username,fullname : dataProfile.name,avatar:dataProfile.avatar_url});
    res.json(profile);
  } catch (error) {
    console.log(error);
    if (error.response.status === 401) {
      res.status(401).json("Unauthorized to access data");
    } else if (error.response.status === 403) {
      res.status(403).json("Permission denied");
    } else {
      res.status(500).json("Whoops, something went wrong");
    }
  }
});

app.listen(port, () => console.log("Started"));
