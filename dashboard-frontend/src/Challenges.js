import React, { useState, useEffect } from "react";
import "./Challenges.css";

import queryString from "query-string";

const Challenges = ({ location }) => {
  const { code } = queryString.parse(location.search);
  const [challengesData, setChallengesData] = useState("none");
  const clientID = 'dab5c84281b8a4fe962187dc85ad0404fde36c0ded67289542e7611d134ebe06'
  const clientSecret = '08c3b41440a80cc14d8c45de9583f01433de6ce036d767148d6b49e8928b0461'
  const redirect_uri = 'https://react-sso-scrumthink.herokuapp.com/challenges'

  useEffect(() => {
    fetch(`https://scrumthink-staging.herokuapp.com/api/loginSSO?client_id=${clientID}&client_secret=${clientSecret}&code=${code}&redirect_uri=${redirect_uri}`, {
      // mode: 'no-cors',
      method: 'GET',
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        Accept: "application/json",
      }
    })
    .then(res => res.json())
    .then(res => {
      // console.log(res.json());
      setChallengesData(JSON.stringify(res));
    }
    )
  }, [code]);

  return (
    <div className="Challenges-body">
      <h3>Challenges</h3>
      <h5 className="Content">{challengesData}</h5>
    </div>
  );
};

export default Challenges;
