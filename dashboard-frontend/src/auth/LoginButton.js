import React from "react";

const LoginButton = () => {
  return (
    <a href="https://git.digitalamoeba.id/oauth/authorize?client_id=dab5c84281b8a4fe962187dc85ad0404fde36c0ded67289542e7611d134ebe06&redirect_uri=https://react-sso-scrumthink.herokuapp.com/challenges&response_type=code&state=08c3b41440a80cc14d8c45de9583f01433de6ce036d767148d6b49e8928b0461&scope=read_user" class="Login-button">Login</a>
  );
};

export default LoginButton;
